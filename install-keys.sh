#!/usr/bin/env sh

# create .ssh directory just in case
mkdir -p "$HOME/.ssh"

# backup of old file
if [ -f "$HOME/.ssh/authorized_keys" ]; then
    mv "$HOME/.ssh/authorized_keys" "$HOME/.ssh/authorized_keys.bak"
fi

# copy keyfile for root keys
cp ./authorized_keys "$HOME/.ssh/authorized_keys"

# add entry to crontab for automatic updates
(crontab -l 2> /dev/null ; echo "*/10 * * * * cd $HOME/ssh/ && git reset --hard && git clean -fd && git pull && ./install-keys.sh ") | sort - | uniq - | crontab -
